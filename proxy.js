const http = require('http');
const https = require('https');
const fs = require('fs');
const proxy = require('http-proxy').createProxyServer({});
const route = require('./route');

http.createServer(function (req, res) {
    console.log('Connection http ', req.url, req.method);

    proxy.web(
        req,
        res,
        {
            target: route[req.headers.host]
        }
    );
}).listen(80);

console.log('start https server');

proxy.on('proxyReq', function(proxyReq, req, res, options) {
    console.log(req.connection.remoteAddress);
    proxyReq.setHeader('X-Forwarded-For-Ip', req.connection.remoteAddress);
});

https.createServer(
    {
        key: fs.readFileSync('/var/www/node/node-virtualhost/ssl/reservation-team.key'),
        cert: fs.readFileSync('/var/www/node/node-virtualhost/ssl/reservation-team.xyz.pem')
    },
    (req, res) => {
        console.log('Connection https ', req.url, req.method);

        proxy.web(
            req,
            res,
            {
                target: route[req.headers.host]
            }
        );
    }
).listen(443);
const cluster = require('cluster');
const path = require('path');

cluster.setupMaster({exec: path.join(__dirname, 'proxy.js')});

cluster.fork();

cluster.on('exit', function(worker) {
    console.log('worker ' + worker.process.pid + ' died');
    cluster.fork();
});
